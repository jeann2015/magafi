<?php 
session_start();
ob_start();
?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php include ("ewconfig.php") ?>
<?php include ("db.php") ?>
<?php include ("bancos_info.php") ?>
<?php include ("advsecu.php") ?>
<?php include ("phpmkrfn.php") ?>
<?php include ("ewupload.php") ?>
<?php
if (!IsLoggedIn()) {
	ob_end_clean();
	header("Location: login.php");
	exit();
}
?>
<?php

// Initialize common variables
$x_codigo = NULL;
$ox_cod_impuesto_municipal = NULL;
$z_cod_impuesto_municipal = NULL;
$ar_x_cod_impuesto_municipal = NULL;
$ari_x_cod_impuesto_municipal = NULL;
$x_codigoList = NULL;
$x_codigoChk = NULL;
$cbo_x_cod_impuesto_municipal_js = NULL;
$x_descripcion = NULL;
$ox_descripcion = NULL;
$z_descripcion = NULL;
$ar_x_descripcion = NULL;
$ari_x_descripcion = NULL;
$x_descripcionList = NULL;
$x_descripcionChk = NULL;
$cbo_x_descripcion_js = NULL;
$x_por_iva = NULL;
$ox_por_iva = NULL;
$z_por_iva = NULL;
$ar_x_por_iva = NULL;
$ari_x_por_iva = NULL;
$x_por_ivaList = NULL;
$x_por_ivaChk = NULL;
$cbo_x_por_iva_js = NULL;
$x_por_ret = NULL;
$ox_por_ret = NULL;
$z_por_ret = NULL;
$ar_x_por_ret = NULL;
$ari_x_por_ret = NULL;
$x_por_retList = NULL;
$x_por_retChk = NULL;
$cbo_x_por_ret_js = NULL;
?>
<?php
$nStartRec = 0;
$nStopRec = 0;
$nTotalRecs = 0;
$nRecCount = 0;
$nRecActual = 0;
$sKeyMaster = "";
$sDbWhereMaster = "";
$sSrchAdvanced = "";
$psearch = "";
$psearchtype = "";
$sDbWhereDetail = "";
$sSrchBasic = "";
$sSrchWhere = "";
$sDbWhere = "";
$sOrderBy = "";
$sSqlMaster = "";
$sListTrJs = "";
$bEditRow = "";
$nEditRowCnt = "";
$sDeleteConfirmMsg = "";
$nDisplayRecs = "20";
$nRecRange = 10;

// Open connection to the database
$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);

// Handle reset command
ResetCmd();

// Set up inline edit parameters
$sAction = "";
SetUpInlineEdit($conn);

// Get search criteria for Basic (Quick) Search
$psearch = (!get_magic_quotes_gpc()) ? addslashes(@$_GET[ewTblBasicSrch]) : @$_GET[ewTblBasicSrch];
$psearchtype = @$_GET[ewTblBasicSrchType];
SetUpBasicSearch();

// Build search criteria
if ($sSrchAdvanced != "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchAdvanced . ")"; // Advanced Search
}
if ($sSrchBasic != "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchBasic . ")"; // Basic Search
}

// Save search criteria
if ($sSrchWhere != "") {
	$_SESSION[ewSessionTblSearchWhere] = $sSrchWhere;

	// Reset start record counter (new search)
	$nStartRec = 1;
	$_SESSION[ewSessionTblStartRec] = $nStartRec;
} else {
	$sSrchWhere = @$_SESSION[ewSessionTblSearchWhere];
	RestoreSearch();
}

// Build filter condition
$sDbWhere = "";
if ($sDbWhereDetail <> "") {
	if ($sDbWhere <> "") $sDbWhere .= " AND ";
	$sDbWhere .= "(" . $sDbWhereDetail . ")";
}
if ($sSrchWhere <> "") {
	if ($sDbWhere <> "") $sDbWhere .= " AND ";
	$sDbWhere .= "(" . $sSrchWhere . ")";
}

// Set up sorting order
$sOrderBy = "";
SetUpSortOrder();
//$sSql = ewBuildSql(ewSqlSelect, ewSqlWhere, ewSqlGroupBy, ewSqlHaving, ewSqlOrderBy, $sDbWhere, $sOrderBy);
$sSql = "select * from bancos order by codigo"

// echo $sSql . "<br>"; // Uncomment to show SQL for debugging
?>
<?php include ("header.php") ?>
<script type="text/javascript">
<!--
EW_LookupFn = "ewlookup.php"; // ewlookup file name
EW_AddOptFn = "ewaddopt.php"; // ewaddopt.php file name

//-->
</script>
<script type="text/javascript" src="ewp.js"></script>
<script type="text/javascript">
<!--
EW_dateSep = "/"; // set date separator
EW_UploadAllowedFileExt = "gif,jpg,jpeg,bmp,png,doc,xls,pdf,zip"; // allowed upload file extension

//-->
</script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {
if (EW_this.x_cod_impuesto_municipal && !EW_hasValue(EW_this.x_cod_impuesto_municipal, "TEXT")) {
	if (!EW_onError(EW_this, EW_this.x_cod_impuesto_municipal, "TEXT", "Por favor ingrese los campos requeridos - C�digo"))
		return false;
}
if (EW_this.x_cod_impuesto_municipal && !EW_checkinteger(EW_this.x_cod_impuesto_municipal.value)) {
	if (!EW_onError(EW_this, EW_this.x_cod_impuesto_municipal, "TEXT", "Entero Incorrecto - C�digo"))
		return false; 
}
if (EW_this.x_por_iva && !EW_checknumber(EW_this.x_por_iva.value)) {
	if (!EW_onError(EW_this, EW_this.x_por_iva, "TEXT", "N�mero de Punto Flotante Incorrecto - % IVA"))
		return false; 
}
if (EW_this.x_por_ret && !EW_checknumber(EW_this.x_por_ret.value)) {
	if (!EW_onError(EW_this, EW_this.x_por_ret, "TEXT", "N�mero de Punto Flotante Incorrecto - % Retenci�n"))
		return false; 
}
return true;
}

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // first data row start at
var tablename = 'ewlistmain'; // table name
var lastrowoffset = 0; // footer row
var usecss = true; // use css
var rowclass = 'ewTableRow'; // row class
var rowaltclass = 'ewTableAltRow'; // row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // row selected class
var roweditclass = 'ewTableEditRow'; // row edit class
var rowcolor = '#FFFFFF'; // row color
var rowaltcolor = '#EEF2F5'; // row alternate color
var rowmovercolor = '#DDEEFF'; // row mouse over color
var rowselectedcolor = '#DDEEFF'; // row selected color
var roweditcolor = '#DDEEFF'; // row edit color

//-->
</script>
<script type="text/javascript">
<!--
	var EW_DHTMLEditors = [];

//-->
</script>
<?php

// Set up recordset

$rs = phpmkr_query($sSql, $conn) or die("Fallo al ejecutar la consulta en la l�nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
$nTotalRecs = phpmkr_num_rows($rs);
if ($nDisplayRecs <= 0) { // Display all records
	$nDisplayRecs = $nTotalRecs;
}
$nStartRec = 1;
SetUpStartRec(); // Set up start record position

?>
<?php if (@$_SESSION[ewSessionMessage] <> "") { ?>
<p><span class="ewmsg"><?php echo $_SESSION[ewSessionMessage]; ?></span>
  <?php $_SESSION[ewSessionMessage] = "";  } ?>
</p>
<table width="100%" class="tb-tit">
  <tr>
    <td class="row-br"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><? tit('bancos') ?></td>
          <td align="right"><table border="0" align="right" cellpadding="0" cellspacing="0" onclick="javascript:window.location='menu_int.php?cod=10';" class="btn_bg" style="cursor: pointer;">
            <tr>
              <td style="padding: 0px;" align="right"><div align="center"><img src="img_sis/bt_left.gif" alt="" width="5" height="20" style="border-width: 0px;" /></div></td>
              <td class="btn_bg"><div align="center"><img src="img_sis/ico_up.gif" width="16" height="16" /></div></td>
              <td class="btn_bg" style="padding: 0px 4px;"><div align="center">Regresar</div></td>
              <td style="padding: 0px;" align="left"><div align="center"><img src="img_sis/bt_right.gif" alt="" width="5" height="20" style="border-width: 0px;" /></div></td>
            </tr>
            <table border="0" cellspacing="0" cellpadding="0" onclick="javascript:window.location='bancos_add.php';">
              <tr>
                <td><?php btn('add','usuarios_add.php') ?></td>
              </tr>
            </table>
          </table>
          <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<table class="ewBasicSearch" width="100%">
<form id="fimpuestos_municipaleslistsrch" name="fimpuestos_municipaleslistsrch" action="bancos.php" >
<table width="100%" class="tb-head">
	<tr><td>
<table>
  <tr>
			<td><input type="text" name="<?php echo ewTblBasicSrch; ?>" size="20" value="<?php echo $psearch;?>"></td>
			<td><? btn('search','fimpuestos_municipaleslistsrch',1) ?></td>
			<td><? btn('show_all','bancos.php?cmd=reset') ?></td>
	    <td><input type="radio" name="<?php echo ewTblBasicSrchType;?>" value="" <?php if ($psearchtype == "") { ?>checked<?php } ?>>Frase exacta&nbsp;<input type="radio" name="<?php echo ewTblBasicSrchType; ?>" value="AND" <?php if ($psearchtype == "AND") { ?>checked<?php } ?>>Todas las palabras&nbsp;&nbsp;<input type="radio" name="<?php echo ewTblBasicSrchType; ?>" value="OR" <?php if ($psearchtype == "OR") { ?>checked<?php } ?>>Cualquier palabra</td>
  </tr>
</table>
	</td></tr>
</form>
</table>
<?php if ($nTotalRecs > 0)  { ?>
<table width="100%"  id="ewlistmain" class="ewTable">
<form name="fimpuestos_municipaleslist" id="fimpuestos_municipaleslist" action="bancos.php" method="post">
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td width="99" valign="top" style="width: 20%;"><div align="left"><span>
          <a href=""> C�digo
          <?php if (@$_SESSION[ewSessionTblSort . "_x_codigo"] == "ASC") { ?>
          <img src="images/sortup.gif" width="10" height="9" border="0">
          <?php } elseif (@$_SESSION[ewSessionTblSort . "_x_cod_impuesto_municipal"] == "DESC") { ?>
          <img src="images/sortdown.gif" width="10" height="9" border="0">
          <?php } ?>
        </a></span></div></td>
		<td width="204" valign="top" style="width: 10%;"><div align="left"><span>
          <a href="">
          Nombre de Banco
          <?php if (@$_SESSION[ewSessionTblSort . "_x_por_iva"] == "ASC") { ?>
          <img src="images/sortup.gif" width="10" height="9" border="0">
          <?php } elseif (@$_SESSION[ewSessionTblSort . "_x_por_iva"] == "DESC") { ?>
          <img src="images/sortdown.gif" width="10" height="9" border="0">
          <?php } ?>
          </a>
        </span></div></td>
		<td width="94" valign="top" style="width: 10%;"><div align="left"><span>
          <a href="">
          Nro. Cuenta 
          <?php if (@$_SESSION[ewSessionTblSort . "_x_por_ret"] == "ASC") { ?>
          <img src="images/sortup.gif" width="10" height="9" border="0">
          <?php } elseif (@$_SESSION[ewSessionTblSort . "_x_por_ret"] == "DESC") { ?>
          <img src="images/sortdown.gif" width="10" height="9" border="0">
          <?php } ?>
          </a>
        </span></div></td>
        <td width="175" valign="top"><div align="left"><span><a href="">Tipo
          <?php if (@$_SESSION[ewSessionTblSort . "_x_por_ret"] == "ASC") { ?>
            <img src="images/sortup.gif" width="10" height="9" border="0">
            <?php } elseif (@$_SESSION[ewSessionTblSort . "_x_por_ret"] == "DESC") { ?>
            <img src="images/sortdown.gif" width="10" height="9" border="0">
            <?php } ?>
        </a> </span></div></td>
        <td><div align="left"><span><a href="">Nro. Cuenta Presupuestaria
          <?php if (@$_SESSION[ewSessionTblSort . "_x_por_ret"] == "ASC") { ?>
            <img src="images/sortup.gif" width="10" height="9" border="0">
            <?php } elseif (@$_SESSION[ewSessionTblSort . "_x_por_ret"] == "DESC") { ?>
            <img src="images/sortdown.gif" width="10" height="9" border="0">
            <?php } ?>
        </a></span></div></td>
        <td><div align="left"></div></td>
<td><div align="left"></div></td>
	</tr>
<?php

// Set the last record to display
$nStopRec = $nStartRec + $nDisplayRecs - 1;

// Move to the first record
$nRecCount = $nStartRec - 1;
if (phpmkr_num_rows($rs) > 0) {
	phpmkr_data_seek($rs, $nStartRec -1);
}
$nEditRowCnt = 0;
$nRecActual = 0;
while (($row = @phpmkr_fetch_array($rs)) && ($nRecCount < $nStopRec)) {
	$nRecCount = $nRecCount + 1;
	if ($nRecCount >= $nStartRec) {
		$nRecActual++;

		// Set row color
		$sItemRowClass = " class=\"ewTableRow\"";
		$sListTrJs = " onmouseover='ew_mouseover(this);' onmouseout='ew_mouseout(this);' onclick='ew_click(this);'";

		// Display alternate color for rows
		if ($nRecCount % 2 <> 1) {
			$sItemRowClass = " class=\"ewTableAltRow\"";
		}
		$x_codigo = $row["codigo"];
		echo "<input type='hidden' id='codigo' name='codigo' value='$x_codigo'>";
		$x_descripcion = $row["descripcion"];
		$x_cuenta = $row["cuenta"];
		$x_tipo = $row["tipo"];
		$x_cuenta_contable = $row["cuenta_contable"];
		
	$bEditRow = (($_SESSION[ewSessionTblKey ."_codigo"] == ((get_magic_quotes_gpc())? stripslashes($x_codigo) : $x_codigo)) && ($nEditRowCnt == 0));
	if ($bEditRow) {
		$nEditRowCnt++;
		$sItemRowClass = " class=\"ewTableEditRow\"";
		$sListTrJs = " onmouseover='this.edit=true;ew_mouseover(this);' onmouseout='ew_mouseout(this);' onclick='ew_click(this);'";
	}
?>
	<!-- Table body -->
	<tr<?php echo $sItemRowClass; ?><?php echo $sListTrJs; ?>>
		<!-- cod_impuesto_municipal -->
		<td style="width: 20%;"><span>
<?php if ($bEditRow) { // Edit record ?>
<?php echo $x_codigo; ?><input type="hidden" id="codigo" name="codigo" value="<?php echo @$x_codigo; ?>">
<?php }else{ ?>
<?php echo $x_codigo; ?>
<?php } ?>
</span></td>
		<!-- descripcion -->
		<!-- por_iva -->
		<td style="width: 10%;"><span>
<?php if ($bEditRow) { // Edit record ?>
<?php }else{ ?>
<?php /*echo (is_numeric($x_por_iva)) ? FormatNumber($x_por_iva,2,-2,-2,-2) : $x_por_iva;*/ echo $x_descripcion; ?>
<?php } ?>
</span></td>
		<!-- por_ret -->
		<td style="width: 10%;"><span>
<?php if ($bEditRow) { // Edit record ?>

<?php }else{ ?>
<?php /*echo (is_numeric($x_por_ret)) ? FormatNumber($x_por_ret,2,-2,-2,-2) : $x_por_ret;*/ echo $x_cuenta; ?>
<?php } ?>
</span></td>
        <td nowrap="nowrap"><p><span>
            <?php if ($bEditRow) { // Edit record ?>
</span><span></span><span>          <?php }else{ ?>
          <?php /*echo (is_numeric($x_por_ret)) ? FormatNumber($x_por_ret,2,-2,-2,-2) : $x_por_ret;*/ echo $x_tipo; ?>
          <?php } ?>
                </span></p>
      </td>
        <td width="274" nowrap="nowrap"><span>
          <?php if ($bEditRow) { // Edit record ?>
          <?php }else{ ?>
          <?php /*echo (is_numeric($x_por_ret)) ? FormatNumber($x_por_ret,2,-2,-2,-2) : $x_por_ret;*/  echo $x_cuenta_contable; ?>
          <?php } ?>
        </span></td>
      <td width="86" nowrap="nowrap">
<?php if ($_SESSION[ewSessionTblKey ."_codigo"] == ((get_magic_quotes_gpc())? stripslashes($x_codigo) : $x_codigo)) { ?>&nbsp;
	  <input type="hidden" name="" value="update">
      <?php } else { ?>
	  <a href="<?php if ($x_codigo <> "") {echo "bancos_edit.php?codigo=" . urlencode($x_codigo); } else { echo "javascript:alert('�Registro Inv�lido! La Clave est� Vac�a');";} ?>"><img src="images/edit.gif" width="15" height="15" alt="Editar" border="0"></a>
      <?php } ?>
</td>
      <td width="25"><div align="center"><span class="phpmaker"><a href="<?php if ($x_codigo <> "") {echo "bancos_delete.php?codigo=" . urlencode($x_codigo); } else { echo "javascript:alert('�Registro Inv�lido! La Clave est� Vac�a');";} ?>"><img src="images/delete.gif" alt="Editar" width="15" height="15" border="0"></a></span></div></td>
	</tr>
<?php
	}
}
?>
</form>
</table>
<?php if (strtolower($sAction) == "edit") { ?>
<?php } ?>
<?php 
}

// Close recordset and connection
phpmkr_free_result($rs);
phpmkr_db_close($conn);
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<form action="bancos.php" name="ewpagerform" id="ewpagerform">
	<tr>
		<td nowrap>
<?php
if ($nTotalRecs > 0) {
	$rsEof = ($nTotalRecs < ($nStartRec + $nDisplayRecs));
	$PrevStart = $nStartRec - $nDisplayRecs;
	if ($PrevStart < 1) { $PrevStart = 1; }
	$NextStart = $nStartRec + $nDisplayRecs;
	if ($NextStart > $nTotalRecs) { $NextStart = $nStartRec ; }
	$LastStart = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
	?>
<table width="100%" class="tb-footer">
  <tr>
    <td>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">P�gina&nbsp;</span></td>
<!--first page button-->
	<?php if ($nStartRec == 1) { ?>
	<td><img src="images/firstdisab.gif" alt="Primera" width="16" height="16" border="0"></td>
	<?php } else { ?>
	<td><a href="bancos.php?start=1"><img src="images/first.gif" alt="Primera" width="16" height="16" border="0"></a></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($PrevStart == $nStartRec) { ?>
	<td><img src="images/prevdisab.gif" alt="Anterior" width="16" height="16" border="0"></td>
	<?php } else { ?>
	<td><a href="bancos.php?start=<?php echo $PrevStart; ?>"><img src="images/prev.gif" alt="Anterior" width="16" height="16" border="0"></a></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="pageno" value="<?php echo intval(($nStartRec-1)/$nDisplayRecs+1); ?>" size="4"></td>
<!--next page button-->
	<?php if ($NextStart == $nStartRec) { ?>
	<td><img src="images/nextdisab.gif" alt="Siguiente" width="16" height="16" border="0"></td>
	<?php } else { ?>
	<td><a href="bancos.php?start=<?php echo $NextStart; ?>"><img src="images/next.gif" alt="Siguiente" width="16" height="16" border="0"></a></td>
	<?php  } ?>
<!--last page button-->
	<?php if ($LastStart == $nStartRec) { ?>
	<td><img src="images/lastdisab.gif" alt="Ultima" width="16" height="16" border="0"></td>
	<?php } else { ?>
	<td><a href="bancos.php?start=<?php echo $LastStart; ?>"><img src="images/last.gif" alt="Ultima" width="16" height="16" border="0"></a></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;de <?php echo intval(($nTotalRecs-1)/$nDisplayRecs+1);?></span></td>
	</tr>
	</table>
	</td>
    <td>
	<?php if ($nStartRec > $nTotalRecs) { $nStartRec = $nTotalRecs; }
	$nStopRec = $nStartRec + $nDisplayRecs - 1;
	$nRecCount = $nTotalRecs - 1;
	if ($rsEof) { $nRecCount = $nTotalRecs; }
	if ($nStopRec > $nRecCount) { $nStopRec = $nRecCount; } ?>
	Registro <?php echo $nStartRec; ?> a <?php echo $nStopRec; ?> de <?php echo $nTotalRecs; ?>
	</td>
  </tr>
</table>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker"></span>
	<?php } else { ?>
	<span class="phpmaker">No se encontraron registros</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</form>
</table>
<?php include ("footer.php") ?>
<?php

//-------------------------------------------------------------------------------
// Function SetUpInlineEdit
// - Set up Inline Edit parameters based on querystring parameters a & key
// - Variables setup: sAction, sKey, Session(TblKeyName)

function SetUpInlineEdit($conn)
{
	global $x_codigo;
	global $bInlineEdit;
	global $sAction;
	global $ewCurSec;

	// Get the keys for master table
	if (strlen(@$_GET["a"]) > 0) {
		$sAction = @$_GET["a"];
		if (strtolower($sAction) == "edit") { // Change to Inline Edit Mode
			$bInlineEdit = true;
			if (strlen(@$_GET["codigo"]) > 0) {
				$x_codigo = $_GET["codigo"];
			} else {
				$bInlineEdit = false;
			}
			if ($bInlineEdit) {
				if (LoadData($conn)) {
					$_SESSION[ewSessionTblKey . "_codigo"] = $x_codigo; // Set up Inline Edit key
				}
			}
		} elseif (strtolower($sAction) == "cancel") { // Switch out of Inline Edit Mode
			$_SESSION[ewSessionTblKey . "_cod_impuesto_municipal"] = ""; // Clear Inline Edit key
		}
	} else {
		$sAction = @$_POST["a_list"];
		if (strtolower($sAction) == "update") { // Update Record

			// Get fields from form
			$GLOBALS["x_codigo"] = @$_POST["x_codigo"];
			$GLOBALS["x_descripcion"] = @$_POST["x_descripcion"];
			$GLOBALS["x_por_iva"] = @$_POST["x_por_iva"];
			$GLOBALS["x_por_ret"] = @$_POST["x_por_ret"];
			if ($_SESSION[ewSessionTblKey ."_codigo"] == ((get_magic_quotes_gpc())? stripslashes($x_codigo) : $x_codigo)) 
			{
				if (InlineEditData($conn)) 
				{
					$_SESSION[ewSessionMessage] = "Actualizaci�n de Registro Exitosa";
				}
			}
		}
		$_SESSION[ewSessionTblKey . "_codigo"] = ""; // Clear Inline Edit key
	}
}

//-------------------------------------------------------------------------------
// Function BasicSearchSQL
// - Build WHERE clause for a keyword

function BasicSearchSQL($Keyword)
{
	$sKeyword = (!get_magic_quotes_gpc()) ? addslashes($Keyword) : $Keyword;
	$BasicSearchSQL = "";
	$BasicSearchSQL.= "`descripcion` LIKE '%" . $sKeyword . "%' OR ";
	if (substr($BasicSearchSQL, -4) == " OR ") { $BasicSearchSQL = substr($BasicSearchSQL, 0, strlen($BasicSearchSQL)-4); }
	return $BasicSearchSQL;
}

//-------------------------------------------------------------------------------
// Function SetUpBasicSearch
// - Set up Basic Search parameter based on form elements pSearch & pSearchType
// - Variables setup: sSrchBasic

function SetUpBasicSearch()
{
	global $sSrchBasic, $psearch, $psearchtype;
	if ($psearch <> "") {
		if ($psearchtype <> "") {
			while (strpos($psearch, "  ") != false) {
				$psearch = str_replace("  ", " ",$psearch);
			}
			$arKeyword = split(" ", trim($psearch));
			foreach ($arKeyword as $sKeyword) {
				$sSrchBasic .= "(" . BasicSearchSQL($sKeyword) . ") " . $psearchtype . " ";
			}
		} else {
			$sSrchBasic = BasicSearchSQL($psearch);
		}
	}
	if (substr($sSrchBasic, -4) == " OR ") { $sSrchBasic = substr($sSrchBasic, 0, strlen($sSrchBasic)-4); }
	if (substr($sSrchBasic, -5) == " AND ") { $sSrchBasic = substr($sSrchBasic, 0, strlen($sSrchBasic)-5); }
	if ($psearch <> "") {
		$_SESSION[ewSessionTblBasicSrch] = $psearch;
		$_SESSION[ewSessionTblBasicSrchType] = $psearchtype;
	}
}

//-------------------------------------------------------------------------------
// Function ResetSearch
// - Clear all search parameters

function ResetSearch() 
{

	// Clear search where
	$sSrchWhere = "";
	$_SESSION[ewSessionTblSearchWhere] = $sSrchWhere;

	// Clear advanced search parameters
	$_SESSION[ewSessionTblAdvSrch . "_x_codigo"] = "";
	$_SESSION[ewSessionTblAdvSrch . "_x_descripcion"] = "";
	$_SESSION[ewSessionTblAdvSrch . "_x_por_iva"] = "";
	$_SESSION[ewSessionTblAdvSrch . "_x_por_ret"] = "";
	$_SESSION[ewSessionTblBasicSrch] = "";
	$_SESSION[ewSessionTblBasicSrchType] = "";
}

//-------------------------------------------------------------------------------
// Function RestoreSearch
// - Restore all search parameters
//

function RestoreSearch()
{

	// Restore advanced search settings
	$GLOBALS["x_codigo"] = @$_SESSION[ewSessionTblAdvSrch . "_x_codigo"];
	$GLOBALS["x_descripcion"] = @$_SESSION[ewSessionTblAdvSrch . "_x_descripcion"];
	$GLOBALS["x_por_iva"] = @$_SESSION[ewSessionTblAdvSrch . "_x_por_iva"];
	$GLOBALS["x_por_ret"] = @$_SESSION[ewSessionTblAdvSrch . "_x_por_ret"];
	$GLOBALS["psearch"] = @$_SESSION[ewSessionTblBasicSrch];
	$GLOBALS["psearchtype"] = @$_SESSION[ewSessionTblBasicSrchType];
}

//-------------------------------------------------------------------------------
// Function SetUpSortOrder
// - Set up Sort parameters based on Sort Links clicked
// - Variables setup: sOrderBy, Session(TblOrderBy), Session(Tbl_Field_Sort)

function SetUpSortOrder()
{
	global $sOrderBy;
	global $sDefaultOrderBy;

	// Check for an Order parameter
	if (strlen(@$_GET["order"]) > 0) {
		$sOrder = @$_GET["order"];

		// Field `cod_impuesto_municipal`
		if ($sOrder == "codigo") {
			$sSortField = "`codigo`";
			$sLastSort = @$_SESSION[ewSessionTblSort . "_x_codigo"];
			$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			$_SESSION[ewSessionTblSort . "_x_codigo"] = $sThisSort;
		} else {
			if (@$_SESSION[ewSessionTblSort . "_x_codigo"] <> "") { @$_SESSION[ewSessionTblSort . "_x_codigo"] = ""; }
		}

		// Field `descripcion`
		if ($sOrder == "descripcion") {
			$sSortField = "`descripcion`";
			$sLastSort = @$_SESSION[ewSessionTblSort . "_x_descripcion"];
			$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			$_SESSION[ewSessionTblSort . "_x_descripcion"] = $sThisSort;
		} else {
			if (@$_SESSION[ewSessionTblSort . "_x_descripcion"] <> "") { @$_SESSION[ewSessionTblSort . "_x_descripcion"] = ""; }
		}

		// Field `por_iva`
		if ($sOrder == "por_iva") {
			$sSortField = "`por_iva`";
			$sLastSort = @$_SESSION[ewSessionTblSort . "_x_por_iva"];
			$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			$_SESSION[ewSessionTblSort . "_x_por_iva"] = $sThisSort;
		} else {
			if (@$_SESSION[ewSessionTblSort . "_x_por_iva"] <> "") { @$_SESSION[ewSessionTblSort . "_x_por_iva"] = ""; }
		}

		// Field `por_ret`
		if ($sOrder == "por_ret") {
			$sSortField = "`por_ret`";
			$sLastSort = @$_SESSION[ewSessionTblSort . "_x_por_ret"];
			$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			$_SESSION[ewSessionTblSort . "_x_por_ret"] = $sThisSort;
		} else {
			if (@$_SESSION[ewSessionTblSort . "_x_por_ret"] <> "") { @$_SESSION[ewSessionTblSort . "_x_por_ret"] = ""; }
		}
		$_SESSION[ewSessionTblOrderBy] = $sSortField . " " . $sThisSort;
		$_SESSION[ewSessionTblStartRec] = 1;
	}
	$sOrderBy = @$_SESSION[ewSessionTblOrderBy];
	if ($sOrderBy == "") {
		if (ewSqlOrderBy <> "" && ewSqlOrderBySessions <> "") {
			$sOrderBy = ewSqlOrderBy;
			@$_SESSION[ewSessionTblOrderBy] = $sOrderBy;
			$arOrderBy = explode(",", ewSqlOrderBySessions);
			for($i=0; $i<count($arOrderBy); $i+=2) {
				@$_SESSION[ewSessionTblSort . "_" . $arOrderBy[$i]] = $arOrderBy[$i+1];
			}
		}
	}
}

//-------------------------------------------------------------------------------
// Function SetUpStartRec
//- Set up Starting Record parameters based on Pager Navigation
// - Variables setup: nStartRec

function SetUpStartRec()
{

	// Check for a START parameter
	global $nStartRec;
	global $nDisplayRecs;
	global $nTotalRecs;
	if (strlen(@$_GET[ewTblStartRec]) > 0) {
		$nStartRec = @$_GET[ewTblStartRec];
		$_SESSION[ewSessionTblStartRec] = $nStartRec;
	} elseif (strlen(@$_GET["pageno"]) > 0) {
		$nPageNo = @$_GET["pageno"];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$_SESSION[ewSessionTblStartRec] = $nStartRec;
		} else {
			$nStartRec = @$_SESSION[ewSessionTblStartRec];
		}
	} else {
		$nStartRec = @$_SESSION[ewSessionTblStartRec];
	}

	// Check if correct start record counter
	if (!(is_numeric($nStartRec)) || ($nStartRec == "")) { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$_SESSION[ewSessionTblStartRec] = $nStartRec;
	} elseif ($nStartRec > $nTotalRecs) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$_SESSION[ewSessionTblStartRec] = $nStartRec;
	}
}

//-------------------------------------------------------------------------------
// Function ResetCmd
// - Clear list page parameters
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters

function ResetCmd()
{

	// Get Reset command
	if (strlen(@$_GET["cmd"]) > 0) {
		$sCmd = @$_GET["cmd"];
		if (strtolower($sCmd) == "reset") { // Reset search criteria
			ResetSearch();
		} elseif (strtolower($sCmd) == "resetall") { // Reset search criteria and session vars
			ResetSearch();
		} elseif (strtolower($sCmd) == "resetsort") { // Reset sort criteria
			$sOrderBy = "";
			$_SESSION[ewSessionTblOrderBy] = $sOrderBy;
			if (@$_SESSION[ewSessionTblSort . "_x_cod_impuesto_municipal"] <> "") { $_SESSION[ewSessionTblSort . "_x_cod_impuesto_municipal"] = ""; }
			if (@$_SESSION[ewSessionTblSort . "_x_descripcion"] <> "") { $_SESSION[ewSessionTblSort . "_x_descripcion"] = ""; }
			if (@$_SESSION[ewSessionTblSort . "_x_por_iva"] <> "") { $_SESSION[ewSessionTblSort . "_x_por_iva"] = ""; }
			if (@$_SESSION[ewSessionTblSort . "_x_por_ret"] <> "") { $_SESSION[ewSessionTblSort . "_x_por_ret"] = ""; }
		}

		// Reset start position (Reset command)
		$nStartRec = 1;
		$_SESSION[ewSessionTblStartRec] = $nStartRec;
	}
}
?>
<?php

//-------------------------------------------------------------------------------
// Function LoadData
// - Variables setup: field variables

function LoadData($conn)
{
	global $x_codigo;
	$sFilter = ewSqlKeyWhere;
	if (!is_numeric($x_codigo)) return false;
	$x_codigo =  (get_magic_quotes_gpc()) ? stripslashes($x_codigo) : $x_codigo;
	$sFilter = str_replace("@bancos", AdjustSql($x_codigo), $sFilter); // Replace key value
	$sSql = ewBuildSql(ewSqlSelect, ewSqlWhere, ewSqlGroupBy, ewSqlHaving, ewSqlOrderBy, $sFilter, "");
	$rs = phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la l�nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	if (phpmkr_num_rows($rs) == 0) 
	{
		$bLoadData = false;
	} 
	else 
	{
		$bLoadData = true;
		$row = phpmkr_fetch_array($rs);
		// Get the field contents
		$GLOBALS["x_codigo"] = $row["codigo"];
		$GLOBALS["x_descripcion"] = $row["descripcion"];
		//$GLOBALS["x_por_iva"] = $row["por_iva"];
		//$GLOBALS["x_por_ret"] = $row["por_ret"];
	}
	phpmkr_free_result($rs);
	return $bLoadData;
}
?>
<?php

//-------------------------------------------------------------------------------
// Function EditData
// - Variables used: field variables

function InlineEditData($conn)
{
	global $x_codigo;
	$sFilter = ewSqlKeyWhere;
	if (!is_numeric($x_codigo)) return false;
	$sTmp =  (get_magic_quotes_gpc()) ? stripslashes($x_codigo) : $x_codigo;
	$sFilter = str_replace("@cod_impuesto_municipal", AdjustSql($sTmp), $sFilter); // Replace key value
	$sSql = ewBuildSql(ewSqlSelect, ewSqlWhere, ewSqlGroupBy, ewSqlHaving, ewSqlOrderBy, $sFilter, "");
	$rs = phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la l�nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);

	// Get old recordset
	$oldrs = phpmkr_fetch_array($rs);
	if (phpmkr_num_rows($rs) == 0) {
		return false; // Update Failed
	} else {
		$x_codigo = @$_POST["x_codigo"];
		$x_descripcion = @$_POST["x_descripcion"];
		$x_por_iva = @$_POST["x_por_iva"];
		$x_por_ret = @$_POST["x_por_ret"];
		$theValue = ($GLOBALS["x_cod_impuesto_municipal"] != "") ? intval($GLOBALS["x_cod_impuesto_municipal"]) : "NULL";
		$fieldList["`codigo`"] = $theValue;
		$theValue = (!get_magic_quotes_gpc()) ? addslashes($GLOBALS["x_descripcion"]) : $GLOBALS["x_descripcion"]; 
		$theValue = ($theValue != "") ? " '" . $theValue . "'" : "NULL";
		$fieldList["`descripcion`"] = $theValue;
		$theValue = ($GLOBALS["x_por_iva"] != "") ? doubleval($GLOBALS["x_por_iva"]) : "NULL";
		$fieldList["`por_iva`"] = $theValue;
		$theValue = ($GLOBALS["x_por_ret"] != "") ? doubleval($GLOBALS["x_por_ret"]) : "NULL";
		$fieldList["`por_ret`"] = $theValue;

		// Updating event
		if (Recordset_Updating($fieldList, $oldrs)) {

			// Update
			$sSql = "UPDATE `impuesto_valoragregado` SET ";
			foreach ($fieldList as $key=>$temp) {
				$sSql .= "$key = $temp, ";
			}
			if (substr($sSql, -2) == ", ") {
				$sSql = substr($sSql, 0, strlen($sSql)-2);
			}
			$sSql .= " WHERE " . $sFilter;
			phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la l�nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
			$result = (phpmkr_affected_rows($conn) >= 0);

			// Updated event
			if ($result) Recordset_Updated($fieldList, $oldrs);
		} else {
			$result = false; // Update Failed
		}
	}
	return $result;
}

// Updating Event
function Recordset_Updating(&$newrs, $oldrs)
{

	// Enter your customized codes here
	return true;
}

// Updated event
function Recordset_Updated($newrs, $oldrs)
{
	$table = "impuesto_valoragregado";
}
?>
