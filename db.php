<?php
function phpmkr_db_connect($HOST, $USER, $PASS, $DB, $PORT)
{
	$conn = mysqli_connect($HOST, $USER, $PASS, $DB, $PORT);
	return $conn;
}
function phpmkr_db_close($conn)
{
	mysqli_close($conn);
}
function phpmkr_query($strsql, $conn)
{
	$rs = mysqli_query($conn, $strsql);
	return $rs;
}
function phpmkr_num_rows($rs)
{
	return @mysqli_num_rows($rs);
}
function phpmkr_fetch_array($rs)
{
	return mysqli_fetch_array($rs);
}
function phpmkr_fetch_row($rs)
{
	return mysqli_fetch_row($rs);
}
function phpmkr_free_result($rs)
{
	@mysqli_free_result($rs);
}
function phpmkr_data_seek($rs, $cnt)
{
	@mysqli_data_seek($rs, $cnt);
}
function phpmkr_error($conn)
{
	return mysqli_error($conn);
}
function phpmkr_insert_id($conn)
{
	return @mysqli_insert_id($conn);
}
function phpmkr_affected_rows($conn)
{
	return @mysqli_affected_rows($conn);
}
?>
<?php
define("HOST", "localhost");
define("PORT", 3306);
define("USER", "root");
define("PASS", "");
define("DB", "magafi");
function AdjustSql($str) {
	$sWrk = trim($str);
	$sWrk = addslashes($sWrk);
	return $sWrk;
}
function ewBuildSql($sSelect, $sWhere, $sGroupBy, $sHaving, $sOrderBy, $sFilter, $sSort) {
	$sDbWhere = $sWhere;

	if ($sDbWhere <> "") {
		$sDbWhere = "(" . $sDbWhere . ")";
	}
	if ($sFilter <> "") {
		if ($sDbWhere <> "") $sDbWhere .= " AND ";
		$sDbWhere .= "(" . $sFilter . ")";
	}
	$sDbOrderBy = $sOrderBy;
	if ($sSort <> "") {
		$sDbOrderBy = $sSort;
	}
	$sSql = $sSelect;
	if ($sDbWhere <> "") {
		$sSql .= " WHERE " . $sDbWhere;
	}
	if ($sGroupBy <> "") {
		$sSql .= " GROUP BY " . $sGroupBy;
	}
	if ($sHaving <> "") {
		$sSql .= " HAVING " . $sHaving;
	}
	If ($sDbOrderBy <> "") {
		$sSql .= " ORDER BY " . $sDbOrderBy;
	}
		//echo $sOrderBy;
	return $sSql;
}
?>
