<?php
function select($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0) {
      global $conn; 
      $rs = $conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select2($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2) 
{
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
	  if ($selecione == 3) {$ret.="<option value='0'>$selecione2</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select3($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3) {
      global $conn; 
      $rs = $conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='$selecione3'>$selecione2</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected >'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};

function select4($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name." disabled>";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select5($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select6($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3) {
      global $conn; 
      $rs = $conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name." onClick='validar();'>";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  //if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};

function select_array($array, $field, $control_name) {
	  $ret="<select class='text-peq' name=".$control_name.">";
	  foreach ($array as $value) {
        if ( $value == $field ) {;
         $ret.='<option value="'.htmlspecialchars($value).'" selected>'.htmlspecialchars($value).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($value).'">'.htmlspecialchars($value).'</option>';
        };
	  };
	  $ret.="</select>";
	 print $ret;
};

function fecha($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY
 if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4);
}

function fecha_en($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY
 if ( ! empty($value) ) return substr($value,5,2) ."/". substr($value,8,2) ."/". substr($value,0,4);
}

function fechahora($value) { // fecha de 'YYYY-MM-DD HH:MM:SS' a 'DD-MM-YYYY HH:MM:SS'
 if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4)." ".substr($value,11,8);
}

function fecha_sql($value) { // fecha de DD/MM/YYYY a YYYYY/MM/DD
 return substr($value,6,4) ."-". substr($value,3,2) ."-". substr($value,0,2);
}

function fechahora_sql($value) { // fecha de 'DD-MM-YYYY HH:MM:SS' a 'YYYY-MM-DD HH:MM:SS'
 return substr($value,6,4) ."-". substr($value,3,2) ."-". substr($value,0,2)." ".substr($value,11,8);
}

function tiempo($value) { // de 'YYYY-MM-DD HH:MM:SS' a 'HH:MM:SS'
 return substr($value,11,8);
}

function numero($value,$dec=0) {
 if ( ! empty($value) ) return number_format($value, 2, ',', '.');
}

function phpmkr_db_connect($archivo, $uso)
{
	//$conn = mysqli_connect($HOST, $USER, $PASS, $DB, $PORT);
	//return $conn;
	$conn = sqlite_open($file, $uso, $sqliteerror)) 
	return $conn;
	echo "se conecto";
}
function phpmkr_db_close($conn)
{
	sqlite_open($conn);
}
function phpmkr_query($strsql, $conn)
{
	//$rs = mysqli_query($conn, $strsql);
	$rs = sqlite_query ($conn, $strsql);
	return $rs;
}
function phpmkr_num_rows($rs)
{
	return sqlite_num_rows($rs);
}
function phpmkr_fetch_array($rs)
{
	return mysqli_fetch_array($rs);
}
function phpmkr_fetch_row($rs)
{
	return mysqli_fetch_row($rs);
}
function phpmkr_free_result($rs)
{
	@mysqli_free_result($rs);
}
function phpmkr_data_seek($rs, $cnt)
{
	@mysqli_data_seek($rs, $cnt);
}
function phpmkr_error($conn)
{
	return sqlite_error_string($conn);
}
function phpmkr_insert_id($conn)
{
	return @mysqli_insert_id($conn);
}
function phpmkr_affected_rows($conn)
{
	return @mysqli_affected_rows($conn);
}
?>
<?php
define("archivo", "magafi");
define("uso", 0666);

function AdjustSql($str) {
	$sWrk = trim($str);
	$sWrk = addslashes($sWrk);
	return $sWrk;
}
function ewBuildSql($sSelect, $sWhere, $sGroupBy, $sHaving, $sOrderBy, $sFilter, $sSort) {
	$sDbWhere = $sWhere;
	if ($sDbWhere <> "") {
		$sDbWhere = "(" . $sDbWhere . ")";
	}
	if ($sFilter <> "") {
		if ($sDbWhere <> "") $sDbWhere .= " AND ";
		$sDbWhere .= "(" . $sFilter . ")";
	}
	$sDbOrderBy = $sOrderBy;
	if ($sSort <> "") {
		$sDbOrderBy = $sSort;
	}
	$sSql = $sSelect;
	if ($sDbWhere <> "") {
		$sSql .= " WHERE " . $sDbWhere;
	}
	if ($sGroupBy <> "") {
		$sSql .= " GROUP BY " . $sGroupBy;
	}
	if ($sHaving <> "") {
		$sSql .= " HAVING " . $sHaving;
	}
	If ($sDbOrderBy <> "") {
		$sSql .= " ORDER BY " . $sDbOrderBy;
	}
	return $sSql;
}
?>
