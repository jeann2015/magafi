<?php
/*
 * @(#) $Header: /var/cvsroot/phphilter/auth.php,v 1.1 2009/06/11 12:30:00 cvs Exp $
 */
/*  
    Copyright (C) 2009- Giuseppe Lucarelli <giu.lucarelli@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of version 2 of the GNU General Public License as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

require('class.phphilter.php');

	$vm = new PhPhilter;
	$vm->Run();

?>
<html>
<head>
<meta http-equiv="Refresh" content="0;url=http://example.org:8080/">
</head>
</html>
